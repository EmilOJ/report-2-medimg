clear all; clc; close all;

%% Projection

%Create phantom
my_phantom = phantom();
figure; 
imagesc(my_phantom);
colormap(gray(128));

num_proj_lines = size(my_phantom, 1);

angles = linspace(0,180,180);
ros = 1:num_proj_lines;

%Preallocation
projection_line = zeros(num_proj_lines,1);
projections = zeros(length(angles), num_proj_lines);

counter = 1;
for angle = angles
    %Roate phantom image
    phantom_rotated = imrotate(my_phantom, angle, 'crop');
    
    for ro = ros
        %Line integral veritically through center of rotated image
        %Repeat for all ro's
        projection_line(ro) = sum(phantom_rotated(:, ro)); 
    end
    
    projections(counter, :) = projection_line;  
    counter = counter + 1;
    
end

%% Plot sinogram
figure; 
imagesc(projections');
colormap(gray(128));
xlabel('\theta in degrees');

%% Backprojection
back_projections = zeros(length(angles), num_proj_lines, num_proj_lines);

% For the kron function, used to spread projection out across the
% backprojcted image

kron_ones = ones(num_proj_lines,1);
counter = 1;
for angle = angles
    current_back_projection = kron(projections(counter,:), kron_ones);
    
    back_projections(counter,:,:) = imrotate(current_back_projection, angle, 'crop');
    counter = counter + 1;
end

back_projection = squeeze(sum(back_projections,1));
back_projection = imrotate(back_projection, 180);
figure;
imagesc(back_projection);
colormap(gray(128));

%% Filtered backprojekction
w_filter = 	abs(linspace(-10, 10, num_proj_lines)) .* blackman(num_proj_lines)';



filtered_back_projections = zeros(length(angles), num_proj_lines, num_proj_lines);
counter = 1;
for angle = angles
    
    
    projection_FT = fftshift(fft(projections(counter, :)));
    projection_FT = w_filter .* projection_FT;
    projection_FT = real((ifft(ifftshift(projection_FT))));
    
    projection_FT_spread = kron(projection_FT, kron_ones);
    filtered_back_projections(counter, :, :) = imrotate(projection_FT_spread, angle, 'crop');
    
    counter = counter + 1;
end

filtered_back_projection = squeeze(sum(filtered_back_projections,1));
filtered_back_projection = flipdim(imrotate(filtered_back_projection, 180), 2);


figure;
imagesc(filtered_back_projection);
colormap(gray(128));

